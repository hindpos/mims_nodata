#!/usr/bin/python2
import json
import os
import re
from lxml import html, etree
from lxml.html.clean import clean_html

#PATH CONSTANTS
DB_DIR = os.path.abspath("temp/complete_db/")+"/"
FINAL_DIR = os.path.abspath("final/")+"/"
PARENT_DIR = "../"
CW_DIR = "./"

def ignorenonprint(list_name):
    return [(''.join([each_char if ord(each_char) < 128 else ' ' for each_char  in item])) for item in list_name]

def getstripppedlist(list_name):
    list_name[:] =  [" ".join(item.strip().split()) for item in list_name if any(character.isalnum() for character in item)]
    list_name[:] = ignorenonprint(list_name)
    list_name[:] =  [" ".join(item.strip().split()) for item in list_name]
    return list_name

def getfolderlist():
    ls_company_folder = []
    ls_all = os.listdir(CW_DIR)
    for ele in ls_all:
        if(os.path.isdir(ele)):
            ls_company_folder.append(ele)
    return ls_company_folder

def parsedrug(drug_file_name):
    fp = open(drug_file_name, "r")
    tree = html.fromstring(fp.read())
    fp.close()
    dict_med_attrib = {}
    list_attrib_name = tree.xpath("//span[@class='xsmallnormaltextblue']/a/text()")
    list_attrib_name[:] = [e.strip() for e in list_attrib_name]
    xpath_pre = "//table"
    list_attrib_val = []
    tab2notpresent = 1
    if(list_attrib_name[-1].startswith("Presentation")):
        tab2notpresent = 0

    for i in range(1, len(list_attrib_name) + tab2notpresent):
        attrib_val = tree.xpath("//table[@id='tbMonoCenter']/tr[" + str(i) + "]/td[@class='outline-all']//text()")
        attrib_val[:] = ignorenonprint(attrib_val)

        if ("Contents" in list_attrib_name[i - 1]):
            new_attrib_val =  [" ".join((item.strip().encode('ascii', 'ignore')).split()) for item in attrib_val if any(character.isalnum() for character in item) and not item.startswith("Click here to")]
            attrib_val = []
            contents_dict = {}
            for ele in new_attrib_val:
                if ele[-1] == ":" and ele[0].isupper():
                    contents_dict["Intake Type"] = ele[:-1]
                else:
                    contents_dict["Chemical Content"] = ele[:-1].strip()
                    attrib_val.append(contents_dict)
                    contents_dict = {}

        elif ("ATC Classification" in list_attrib_name[i - 1]):
            new_attrib_val = []
            for ele in attrib_val:
                ele = ele.strip().encode('ascii', 'ignore')
                if any(character.isalnum() for character in ele):
                    if(ele[-1] == '-'):
                        ele = (ele[:-1]).strip()
                    if(ele[0] == ';'):
                        ele = (ele[1:]).strip()
                    new_attrib_val.append(ele)
            keys =  ["ATC Code", "Generic", "Description"]
            attrib_val = [dict([(keys[key_type], new_attrib_val[3 * each_class + key_type]) for key_type in range(3)]) for each_class in range(len(new_attrib_val)/3)]

        elif ("Preg Safety" in list_attrib_name[i - 1]):
            total_groups = len(tree.xpath("//table[@id='tbMonoCenter']/tr[" + str(i) + "]/td[@class='outline-all']/*"))
            attrib_val = []
            attrib_dict = {}
            for text_group_num in range(1, total_groups + 1):
                text_group = tree.xpath("//table[@id='tbMonoCenter']/tr[" + str(i) + "]/td[@class='outline-all']/*[" + str(text_group_num) + "]//text()")
                stripped_text_group = [" ".join((item.strip().encode('ascii', 'ignore')).split()) for item in text_group if any(character.isalnum() for character in item) and len(item) > 1]
                if len(stripped_text_group) == 0:
                    continue
                if stripped_text_group[0].startswith("ROUTE(S)"):
                    attrib_dict["Route(s)"] = ("".join((("".join(stripped_text_group)).split(":"))[1:])).strip()
                elif stripped_text_group[0].startswith('Category'):
                    attrib_dict["Category"] = (stripped_text_group[0])[-1]
                    attrib_dict["Category Description"] = (("".join(stripped_text_group[1:]))[1:]).strip()
                    attrib_val.append(attrib_dict)
                    attrib_dict = {}
                elif "aution" in stripped_text_group[0]:
                    attrib_dict["Caution"] = "".join(stripped_text_group)
                else:
                    condicat = "".join(stripped_text_group)
                    attrib_dict["Conditional Category"] = { "Category" : condicat[0], "Trimester(s)" : (condicat[5:]).strip() }
        else:
            attrib_val = " ".join(("".join(attrib_val).encode('ascii', 'ignore')).split())
        list_attrib_val.append(attrib_val)

    if(tab2notpresent == 0):
        packing_list=[]
        form_list = tree.xpath("//td[@width = '34%'][@class = 'outline-all']/font/strong/text()")
        form_list[:] = [e.strip() for e in form_list]
        for i in range(0, len(form_list)):
            packing_dict={}
            packing_dict["Form"] = form_list[i]
            med_desc_list = tree.xpath("(//td[@colspan='2'][@class='outline-all'])[" + str(i + 1) + "]//text()")
            med_desc_list_json = []
            for med_desc in med_desc_list:
                packing = re.sub("\(.*\)", "", med_desc).strip().encode('ascii', 'ignore')
                mrp = re.search("\(.*\)", med_desc)
                med_desc_dict = {}
                if packing:
                    med_desc_dict["Packing"] = packing
                if mrp:
                    mrp = mrp.group(0)[1:-1]
                    med_desc_dict["MRP"] = mrp.encode('ascii', 'ignore')
                med_desc_list_json.append(med_desc_dict)
            packing_dict["Description"] = med_desc_list_json
            packing_list.append(packing_dict)
        list_attrib_val.append(packing_list)
    drug_dict =  dict(zip(list_attrib_name, list_attrib_val))
    drug_dict["Drug Name"] = (tree.xpath("//h1[@class = 'monographbrandheadertext']/span[2]//text()")[0]).encode('ascii', 'ignore')
    del drug_dict["Manufacturer"]
    return drug_dict

def parsecompany(company_folder_name):
    print " +++ Parsing " + company_folder_name + " ..." ,
    js = open(FINAL_DIR + company_folder_name.replace(" Products & Contact Information | CIMS India", "") + ".json", "w") #Create a json for the company
    os.chdir(company_folder_name) #Enter the company folder

    company_file = open("company_page.html", "r") #Open the html file of the company
    tree = html.fromstring(company_file.read()) #The tree of the html file of the company
    company_file.close()
    dict_com_attrib = {} #The dictionary containing all the attributes of the company *** level 0
    dict_com_attrib["Company Name"] = tree.xpath("//td[@class = 'normaltext']/h1/text()")[0].strip().encode('ascii', 'ignore')

    extra_details_name =  tree.xpath("//td[@class = 'normaltext'][descendant::section]/text()")
    extra_details_name[:] = getstripppedlist(extra_details_name)
    extra_details_name[:] = [name[:-1] for name in extra_details_name] #The list of all extra details of the company, e.g., "Tel", "Fax", "Email", etc.

    postal_address_val = tree.xpath("//td[@class = 'normaltext']/section/span[1]//text()")
    postal_address_val[:] = getstripppedlist(postal_address_val) #The list containg postal address of the company

    extra_details_val = []
    if len(postal_address_val):
        dict_com_attrib["Postal Address"] = " ".join(postal_address_val) #Add postal address entry to dict only if present

    for index_extra_det in range(1, len(extra_details_name) + 1):
        extra_details = tree.xpath("//td[@class = 'normaltext'][descendant::section]/span[" + str(index_extra_det)  + "]//text()")
        str_exra_details = ",".join(extra_details)
        extra_details = str_exra_details.split(",") #Get list of the extra details *** level 1
        extra_details[:] = getstripppedlist(extra_details)
        extra_details_val.append(extra_details)

    dict_com_attrib.update(dict(zip(extra_details_name, extra_details_val))) #Add extra details to company attributes

    ls_all = os.listdir(".")
    ls_file=[] #list of drug files
    if not ls_all:
        return
    for ele in ls_all:
        if "CIMS" in ele :
            ls_file.append(ele) #Add only drug files
        if os.stat(ele).st_size == 0:
            os.chdir(PARENT_DIR)
            print "Incomplete download ... downloaded before %s" % ele
            return

    druglist = [] #list of all drugs and their attributes
    if not ls_file:
        return
    for drug_file_name in ls_file:
        druglist.append(parsedrug(drug_file_name))
    dict_com_attrib["Drugs"] = druglist
    js.write(json.dumps(dict_com_attrib, indent = 2, sort_keys = True))
    js.close()

    print "Done!"

    os.chdir(PARENT_DIR)

def parseall():
    os.chdir(DB_DIR) #Enter the database directory
    ls_char_folder = getfolderlist()
    for character in ls_char_folder: #Visit every character from 'a' to 'z'
        os.chdir(character) #Enter the director for that character
        ls_company_folder = getfolderlist() #Get the list of all companies starting with the character
        for company_folder in ls_company_folder:
            parsecompany(company_folder) #Parse each company
        os.chdir(PARENT_DIR) #Get back to the database directory

if __name__ == "__main__":
    parseall()
