#!/usr/bin/python2
import mechanize
import re
import cookielib
import os
import json
from lxml import html, etree
from lxml.html.clean import clean_html
from create_json import *

#GLOBAL
now_char = st_char = 'a'
now_page = st_page = 1
now_company = st_company = 0
now_drug = st_drug = 0
startpointfilename = "startingpoint.json"

#CONSTANTS
AUTH_DICT = {"username" : "nascarsayan@gmail.com", "password" : "abcd1234"}
BROWSER_HEADERS = [('user-agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/58.0.3029.81 Chrome/58.0.3029.81 Safari/537.36'), ('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')]
URL_SNIPPET = {"mims" : "http://www.mims.com/", "alphabet" : "/india/browse/alphabet", "company" : "/india/company/info", "drug" : "/india/drug/info"}

#PATH CONSTANTS
PARENT_DIR = ".."
TEMP_DIR = os.path.abspath("temp/")

def checkedchangedir(dirpath):
    if not (os.path.exists(dirpath)):
        os.mkdir(dirpath)
    os.chdir(dirpath)

def handle_connection_error(e):
    global now_char, now_page, now_company, now_drug, startpointfilename
    print e
    with open(startpointfilename, "w") as startpointfile:
        startpointfile.write(json.dumps({"character":now_char, "page_no":now_page, "company_no":now_company, "drug_no":now_drug}, indent = 2))
        exit()

def getstartpoint():
    global st_char, st_page, st_company, st_drug, now_char, now_page, now_company, now_drug, startpointfilename
    if (not os.path.isfile(startpointfilename)) or (os.stat(startpointfilename).st_size == 0):
        with open(startpointfilename, "w") as startpointfile:
            startpointfile.write(json.dumps({"character":'a', "page_no":1, "company_no":0, "drug_no":0}, indent = 2))
    with open(startpointfilename, "r") as startpointfile:
        data = startpointfile.read()
        data_dic = json.loads(data)
        print "Starting Point : "
        print json.dumps(data_dic, indent = 2)
        now_char = st_char = data_dic['character']
        now_page = st_page = data_dic['page_no']
        now_company = st_company = data_dic['company_no']
        now_drug = st_drug = data_dic['drug_no']

def visitdrugpage(browser):
    global st_drug, now_drug
    try:
        druglinks = browser.links(url_regex=re.compile(URL_SNIPPET["drug"]))
        total_drug = len(list(druglinks))  #total drug links
        print "Total Drug : " + str(total_drug)
        len_total_drug = len(str(total_drug - 1))
        for now_drug in range(st_drug, total_drug):
            browser.follow_link(url_regex=re.compile(URL_SNIPPET["drug"]), nr = now_drug)
            print "D {: <{}} ".format(now_drug, len_total_drug) + "({: >{}}) : ".format(now_drug + 1 - total_drug, len_total_drug + 1) + browser.geturl()
            with open(browser.title().replace("/", "."), "w") as f:
                f.write((clean_html(browser.response().read())).strip().replace("\r\n", ""))
            browser.back()
    except (Exception, KeyboardInterrupt) as e:
        handle_connection_error(e)

def visitcompanypage(browser):
    global st_company, now_company, st_drug
    try:
        companylinks = browser.links(url_regex=re.compile(URL_SNIPPET["company"]))
        total_company = len(list(companylinks))  #total company links
        print "Total Company : " + str(total_company)
        len_total_company = len(str(total_company - 1))
        for now_company in range(st_company, total_company):
            browser.follow_link(url_regex=re.compile(URL_SNIPPET["company"]), nr = now_company)
            company_folder = browser.title().replace("/", ".")
            print "C {: <{}} ".format(now_company, len_total_company) + "({: >{}}) : ".format(now_company + 1 - total_company, len_total_company + 1) + browser.geturl()
            checkedchangedir(company_folder)
            with open("company_page.html", "w") as f:
                f.write((clean_html(browser.response().read())).strip().replace("\r\n", ""))
            visitdrugpage(browser)
            st_drug = 0
            browser.back()
            os.chdir(PARENT_DIR)
            parsecompany(company_folder)
    except (Exception, KeyboardInterrupt) as e:
        handle_connection_error(e)

def authorize(browser):
    try:
        browser.select_form("login-form")
        browser["SignInEmailAddress"] = AUTH_DICT["username"]
        browser["SignInPassword"] = AUTH_DICT["password"]
        browser.submit()
        print "Logged in as " + AUTH_DICT["username"] + " : " + browser.geturl()
    except (mechanize.HTTPError, mechanize.URLError, KeyboardInterrupt) as e:
        handle_connection_error(e)

def visitcompanylist(browser, alphabet, page):
    try:
        base_url = URL_SNIPPET["mims"] + "india/browse/alphabet/" + str(alphabet) + "?cat=company&page="+str(page)
        page_number_regex = URL_SNIPPET["alphabet"] + "/.*&page="+str(page) #regex to navigate to the particular page
        cookiejar = cookielib.LWPCookieJar()
        browser.set_cookiejar(cookiejar)
        browser.set_handle_redirect(True)
        browser.addheaders = BROWSER_HEADERS
        browser.open("https://sso.mims.com/Account/SignIn", timeout = 100)
        authorize(browser)
        browser.open(base_url, timeout = 100)
        #Redirection Takes two Empty form submissions
        browser.form = list(browser.forms())[0]
        browser.submit()
        browser.form = list(browser.forms())[0]
        browser.submit()
        links_to_page_no = browser.links(url_regex=re.compile(page_number_regex))
        if ( page > 1):
            browser.follow_link(url=list(links_to_page_no)[0].url)
    except (Exception, KeyboardInterrupt) as e:
        handle_connection_error(e)

def visitcharacter():
    global now_char, now_page, st_page, st_company
    checkedchangedir(now_char)
    browser = mechanize.Browser()
    visitcompanylist(browser, now_char, 1)
    tree = html.fromstring((clean_html(browser.response().read())).strip())
    total_page = len(("".join(tree.xpath("//table[2]/tr[2]//text()"))).split())
    if not total_page:
        total_page = 1
    len_total_page = len(str(total_page))
    print "Total Page : " + str(total_page)
    for now_page in range(st_page, total_page + 1) :
        visitcompanylist(browser, now_char, now_page)
        try:
            browser.follow_link(url_regex=re.compile(URL_SNIPPET["company"]))
        except (Exception, KeyboardInterrupt) as e:
            handle_connection_error(e)
        #Capcha can be countered by a back which causes exception and then two backs
        try:
            browser.back()
        except:
            browser.back()
            browser.back()
        finally:
            print "P {: <{}} ".format(now_page, len_total_page) + "({: >{}}) : ".format(now_page - total_page, len_total_page + 1) + browser.geturl()
            visitcompanypage(browser)
            st_company = 0
    browser.close()
    os.chdir(PARENT_DIR)

def visitall():
    global now_char, st_page, st_char
    checkedchangedir(TEMP_DIR)
    global startpointfilename
    startpointfilename = os.path.abspath("startingpoint.json")
    checkedchangedir("complete_db")
    getstartpoint()
    if st_char == "done":
        print "All downloaded"
        return
    if len(st_char) == 1:
        for now_char in list(map(chr, range(ord(st_char), 123))):
            print "Character : " + now_char
            visitcharacter()
            st_page = 1
        now_char = "0-9"
    print "Character : " + now_char
    visitcharacter()
    now_char = "done"


def verifycompanydownloads():
    checkedchangedir(TEMP_DIR + "/complete_db")
    browser = mechanize.Browser()
    visitcompanylist(browser, "a", 1)
    for check_char in list(map(chr, range(97, 123))):
        print "Character : " + check_char
        char_url = URL_SNIPPET["mims"] + "india/browse/alphabet/" + str(check_char) + "?cat=company&page=1"
        browser.open(char_url, timeout = 100)
        total_company_down = len(os.listdir(check_char))
        tree = html.fromstring((clean_html(browser.response().read())).strip())
        total_company_in_server = int(((tree.xpath("//*[@id='content']/table[2]/tr[1]/td//text()"))[0].split(":"))[-1].strip())
        print "S " + str(total_company_in_server)
        print "D " + str(total_company_down)
        #print "Total down = %d\nTotal serv = %d" %total_company_down %total_company_in_server
        if(total_company_in_server != total_company_down):
            print "@@@@@@@@@ Not downloaded Character %s" %check_char

def verifydrugdownloads():
    checkedchangedir(TEMP_DIR + "/complete_db")
    for check_char in list(map(chr, range(97, 123))):
        os.chdir(check_char)
        ls_company = os.listdir(".")
        for company in ls_company:
            total_drug_down = len(os.listdir(company)) - 1
            tree = ""
            with open(company + "/company_page.html") as fp:
                tree = html.fromstring(fp.read())
            total_drug_in_server = max(len(list(tree.xpath("//li//font[@class = 'normaltext']"))), len(list(tree.xpath("//a[starts-with(@href, '/india/drug/info')]"))))
            if(total_drug_in_server != total_drug_down):
                print "S %u D %u" % (total_drug_in_server, total_drug_down)
                print "Company = %s" % company
        os.chdir(PARENT_DIR)

if __name__ == "__main__":
    #visitall()
    #verifycompanydownloads()
    verifydrugdownloads()
